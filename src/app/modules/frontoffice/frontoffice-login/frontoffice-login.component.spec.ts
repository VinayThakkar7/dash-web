import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontofficeLoginComponent } from './frontoffice-login.component';

describe('FrontofficeLoginComponent', () => {
  let component: FrontofficeLoginComponent;
  let fixture: ComponentFixture<FrontofficeLoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontofficeLoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontofficeLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
