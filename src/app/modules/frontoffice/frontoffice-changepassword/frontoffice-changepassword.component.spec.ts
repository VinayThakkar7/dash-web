import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontofficeChangepasswordComponent } from './frontoffice-changepassword.component';

describe('FrontofficeChangepasswordComponent', () => {
  let component: FrontofficeChangepasswordComponent;
  let fixture: ComponentFixture<FrontofficeChangepasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrontofficeChangepasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontofficeChangepasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
