import { Component, OnInit } from '@angular/core';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewModelComponent } from 'src/app/shared/view-model/view-model.component';

@Component({
  selector: 'app-backoffice-login',
  templateUrl: './backoffice-login.component.html',
  styleUrls: ['./backoffice-login.component.scss']
})
export class BackofficeLoginComponent implements OnInit {
minDate = new Date();
maxDate = new Date(2020, 3, 10);
  constructor( private modalService: NgbModal) {
     }

  ngOnInit(): void {
  }
  open() {
    const modalRef = this.modalService.open(ViewModelComponent);
  }
} 