import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackofficeChangepasswordComponent } from './backoffice-changepassword.component';

describe('BackofficeChangepasswordComponent', () => {
  let component: BackofficeChangepasswordComponent;
  let fixture: ComponentFixture<BackofficeChangepasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BackofficeChangepasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackofficeChangepasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
