import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BackofficeLoginComponent } from './backoffice-login/backoffice-login.component';

const routes: Routes = [
  {path: 'login', component: BackofficeLoginComponent},
  {path: '', component:BackofficeLoginComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackofficeRoutingModule { }
