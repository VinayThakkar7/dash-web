import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackofficeRoutingModule } from './backoffice-routing.module';
import { ViewModelComponent } from 'src/app/shared/view-model/view-model.component';
import { BackofficeLoginComponent } from './backoffice-login/backoffice-login.component';
import { BackofficeChangepasswordComponent } from './backoffice-changepassword/backoffice-changepassword.component';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [  
   BackofficeLoginComponent,
  BackofficeChangepasswordComponent],
  imports: [
    CommonModule,
    BackofficeRoutingModule,
    SharedModule
  
  ],
   entryComponents: [ViewModelComponent],
 
})
export class BackofficeModule { }
