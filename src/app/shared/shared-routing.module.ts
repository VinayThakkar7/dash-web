import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TableImplementationComponent } from './table-implementation/table-implementation.component';
import { TableComponent } from './table/table.component';

const routes: Routes = [
  {path: 'table', component:TableImplementationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
