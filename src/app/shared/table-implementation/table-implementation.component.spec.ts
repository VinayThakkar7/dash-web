import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableImplementationComponent } from './table-implementation.component';

describe('TableImplementationComponent', () => {
  let component: TableImplementationComponent;
  let fixture: ComponentFixture<TableImplementationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableImplementationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableImplementationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
