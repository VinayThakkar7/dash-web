import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngb-modal';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { SharedRoutingModule } from './shared-routing.module';
import { TableImplementationComponent } from './table-implementation/table-implementation.component';
import { TableComponent } from './table/table.component';
import { ViewModelComponent } from './view-model/view-model.component';

@NgModule({
  imports: [AngularMaterialModule,SharedRoutingModule,CommonModule,
    NgbModule],
  exports: [AngularMaterialModule,  TableComponent],
  declarations: [
    
    ViewModelComponent,
    TableImplementationComponent,
    TableComponent
  ],
 
})
export class SharedModule { }
