import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  columnNames: string[] = [];
    @Input() enableCheckbox: boolean;
    @Input() allowMultiSelect: boolean;
    @Input() sqColumnDefinition: ColumnSettingsModel[];
    @Input() sqPaginationConfig: TablePaginationSettingsModel;
    @Input() rowData: object[];
    selection = new SelectionModel<{}>();
    dataSource: MatTableDataSource<{}>;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @Output() getSelectedRows = new EventEmitter();
    isHasTable:boolean;
    ngAfterViewInit() {
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
    }


    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }
   masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
        this.getSelectedRows.emit(this.selection.selected);

    }
    rowSelect() {
        this.getSelectedRows.emit(this.selection.selected);
    }
    ngOnInit() {
      this.dataSource = new MatTableDataSource(this.rowData);
      if(this.dataSource.data.length > 0 ){
        this.isHasTable = true
      }else{
        this.isHasTable = false
      }
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        for (const column of this.sqColumnDefinition) {
            this.columnNames.push(column.name);
        }
        if (this.enableCheckbox) {
            this.columnNames.splice(0, 0, 'select');
            this.sqColumnDefinition.splice(0, 0, {
                'name': 'select',
                'displayName': '#'
            });
        }
        
        this.selection = new SelectionModel<{}>(this.allowMultiSelect, []);
        this.dataSource = new MatTableDataSource(this.rowData);
    }
   
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}

export interface TablePaginationSettingsModel {
enablePagination: boolean;
  pageSize: number;
  pageSizeOptions: number[];
  showFirstLastButtons: boolean;
}export interface ColumnSettingsModel {
  icon?: string;
  name: string;
  displayName: string;
  disableSorting?: boolean;
  toggleButton?:boolean;
}
