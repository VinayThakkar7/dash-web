import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewModelComponent } from './shared/view-model/view-model.component';

const routes: Routes = [
  {
    path: 'backoffice',
    loadChildren: () => import('./modules/backoffice/backoffice.module').then(module => module.BackofficeModule),
    
  },
  {
    path: 'share',
    loadChildren: () => import('./shared/shared.module').then(module => module.SharedModule),
    
  },
  {
    path: '',
    loadChildren: () => import('./modules/backoffice/backoffice.module').then(module => module.BackofficeModule),
    
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
