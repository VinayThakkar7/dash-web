import { HttpClient, HttpEvent, HttpEventType, HttpRequest } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ConstantKey } from './core-helper.classes';

@Injectable({
  providedIn: 'root'
})
export class CoreHelperService {
  downloadStatus: any;
  fileName: string = ""
  constructor(private titleService: Title, private route: Router, private http: HttpClient, private toast: ToastrService) {
    this.downloadStatus = new EventEmitter<ProgressStatusEnum>();
   }


  private viewWarehouseDetails = new BehaviorSubject<Object>(false);
  warehouseDetailsData = this.viewWarehouseDetails.asObservable();

  //constructor(private titleService: Title, private route: Router) { }

  
  setRootData = ()=>{
    this.viewWarehouseDetails.next(true);
  }
  setBrowserTabTitle = (title: string) => {
    this.titleService.setTitle(title);
  }

  isNullOrUndefined<T>(tObj: T): boolean {
    return tObj === null || tObj === undefined;
  }

  checkDuplicateData<T>(ObjList :Array<T>, prop : string,value : T) : boolean{
    let exists = _.find(ObjList, (x:T) =>{
      // return x[prop] == value
    });
    return this.isNullOrUndefined(exists);
  }

  // isNullOrUndefinedMultiple(...tObj): boolean {
  //   return !tObj.every((tEntry) => tEntry !== null && tEntry !== undefined);
  // }

  isStringEmptyOrWhitespace(stringToParse: string) {
    return this.isNullOrUndefined(stringToParse) || stringToParse.trim() === '';
  }

  isArrayEmpty<T>(tArr: T[]): boolean {
    return this.isNullOrUndefined(tArr) || tArr.length <= 0;
  }

  getShortedText = (text: string, limit: number) => {
    if (!this.isNullOrUndefined(limit)) {
      return text.length > limit ? text.substring(0, limit) + '...' : text;
    }
    return text.length > ConstantKey.STRINGLIMITSHORT ? text.substring(0, ConstantKey.STRINGLIMITSHORT) + '...' : text;
  }

  removeAllWhiteSpaces = (text: string) => {
    return text.replace(/\s/g, '');
  }

  snakeCaseToLowerCase = (text: string) => {
    return text.replace(/\_/g, '');
  }

  minSelectedCheckboxes(min = 1) {
    const validator: ValidatorFn = (formArray: AbstractControl) => {
      if (formArray instanceof FormArray) {
        const totalSelected = formArray.controls.length;
        return totalSelected >= min ? null : { notSelected: true };
      }

      throw new Error('formArray is not an instance of FormArray');
    };

    return validator;
  }
  maxSelectedCheckboxes(max : number) {
    const validator: ValidatorFn = (formArray: AbstractControl) => {
      if (formArray instanceof FormArray) {
        const totalSelected = formArray.controls.length;
        return totalSelected > max ? { maxInvalid: true } : null;
      }

      throw new Error('formArray is not an instance of FormArray');
    };

    return validator;
  }

  getLoggedinUserDetail = () => {
    var userData = localStorage.getItem('currentUser');
    if (userData !== null && userData !== undefined && userData !== "") {
      return JSON.parse(userData);
    }
    else {
      this.route.navigate(['/auth']);
    }
  }


  getLoggedinUserDetailWithoutRoute = ()=>{
    var userData = localStorage.getItem('currentUser');
    if (userData !== null && userData !== undefined && userData !== "") {
      return JSON.parse(userData);
    }
    else
    {
      return null;
    }
  }
  getLoggedinAdmin = () => {
    var adminData = localStorage.getItem('adminDetail');
      if (adminData !== null && adminData !== undefined && adminData !== "") {
      return JSON.parse(adminData);
    }
    else {
      this.route.navigate(['/admin']);
    
    }
    
  }
  patternPasswordValidator(): ValidatorFn {
    return (control: AbstractControl): any => {
      if (control.value) {

        const regex = new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})')
        const valid = regex.test(control.value);
        if (!valid) {
          return { invalidPassword: true };
        }
        // return valid ?  null : { invalidPassword: true };
      }
      //const regex = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$');
    };
  }
  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }
  MustNotMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      // if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      //   // return if another validator has already found an error on the matchingControl
      //   return;
      // }

      // set error on matchingControl if validation fails
      if (control.value == matchingControl.value) {
        matchingControl.setErrors({ mustNotMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  convertToTitleCase(str: any) {
    str = str.toLowerCase();
    str = str.split(' ');
    for (var i = 0; i < str.length; i++) {
      str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
    }
    // Step 4. Return the output
    return str.join(' ');
  }
  public GetDownloadFileBlob(file: string): Observable<HttpEvent<Blob>> {
    return this.http.request(new HttpRequest(
    'GET', `${environment.apiUrl}/api/common/Downloadfile?file=${file}`,
    null,
    {
    reportProgress: true,
    responseType: 'blob'
    }));
    }
  public downloadFile(pdfFileName: string, isPdfOpen: boolean = false) {
    this.fileName = pdfFileName?.split("\\")?.pop()!;
    this.GetDownloadFileBlob(pdfFileName).subscribe(
    (data: any) => {
    switch (data.type) {
    case HttpEventType.DownloadProgress:
    this.downloadStatus.emit({ status: ProgressStatusEnum.IN_PROGRESS, percentage: Math.round((data.loaded / data.total) * 100) });
    break;
    case HttpEventType.Response:
    this.downloadStatus.emit({ status: ProgressStatusEnum.COMPLETE });
    
    if (isPdfOpen) {
    var file = new Blob([data.body], { type: data.body.type });
    var fileURL = URL.createObjectURL(file);
    window.open(fileURL);
    }
    else {
    
    const downloadedFile = new Blob([data.body], { type: data.body.type });
    const a = document.createElement('a');
    a.setAttribute('style', 'display:none;');
    document.body.appendChild(a);
    a.download = this.fileName
    a.href = URL.createObjectURL(downloadedFile);
    a.target = '_blank';
    a.click();
    document.body.removeChild(a);
    }
    
    break;
    }
    },
    error => {
    this.downloadStatus.emit({ status: ProgressStatusEnum.ERROR });
    this.toast.error("File Not Found");
    }
    );
    }
}
export enum ProgressStatusEnum {
  START, COMPLETE, IN_PROGRESS, ERROR
  }