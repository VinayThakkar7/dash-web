import { NgModule } from '@angular/core';

import { CoreAuthenticationGuard } from './core-authentication.guard';

@NgModule({
  providers: [
    CoreAuthenticationGuard
  ]
})
export class CoreAuthenticationModule { }
