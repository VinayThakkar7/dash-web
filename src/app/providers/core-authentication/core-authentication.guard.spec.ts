import { TestBed, async, inject } from '@angular/core/testing';

import { CoreAuthenticationGuard } from './core-authentication.guard';

describe('CoreAuthenticationGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CoreAuthenticationGuard]
    });
  });

  it('should ...', inject([CoreAuthenticationGuard], (guard: CoreAuthenticationGuard) => {
    expect(guard).toBeTruthy();
  }));
});
