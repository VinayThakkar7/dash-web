import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';

import { CoreAuthenticationModule } from './core-authentication/core-authentication.module';
import { CoreHelperModule } from './core-helper/core-helper.module';
import { CoreHttpModule } from './core-http/core-http.module';
import { CoreInterceptorModule } from './core-interceptor/core-interceptor.module';


@NgModule({
  declarations: [],
  imports: [
    CoreHttpModule,
    CoreInterceptorModule,
    CoreAuthenticationModule,
    CoreHelperModule
  ]
})
export class CoreModule { }
