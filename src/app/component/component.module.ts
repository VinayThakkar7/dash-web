import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentRoutingModule } from './component-routing.module';
import { BackofficeLoginComponent } from '../modules/backoffice/backoffice-login/backoffice-login.component';
import { BackofficeFooterComponent } from './backoffice-footer/backoffice-footer.component';
import { BackofficeHeaderComponent } from './backoffice-header/backoffice-header.component';
import { BackofficeSidebarComponent } from './backoffice-sidebar/backoffice-sidebar.component';
import { FrontofficeFooterComponent } from './frontoffice-footer/frontoffice-footer.component';
import { FrontofficeHeaderComponent } from './frontoffice-header/frontoffice-header.component';


@NgModule({
  declarations: [
    BackofficeLoginComponent,
    FrontofficeHeaderComponent,
    FrontofficeFooterComponent,
    BackofficeHeaderComponent,
    BackofficeFooterComponent,
    BackofficeSidebarComponent
  ],
  imports: [
    CommonModule,
    ComponentRoutingModule
  ],
  exports:[
    BackofficeLoginComponent,
    FrontofficeHeaderComponent,
    FrontofficeFooterComponent,
    BackofficeHeaderComponent,
    BackofficeFooterComponent,
    BackofficeSidebarComponent
  ]
})
export class ComponentModule { }
